#!/bin/bash
source env-build-fenics.sh

SLEPC_VERSION="3.5.3"
export PETSC_DIR=${PREFIX}

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget -nc http://www.grycap.upv.es/slepc/download/download.php?filename=slepc-${SLEPC_VERSION}.tar.gz -O slepc-${SLEPC_VERSION}.tar.gz && \
   tar -xf slepc-${SLEPC_VERSION}.tar.gz && \
   cd slepc-3.5.3 && \
   ./configure --prefix=${PREFIX} && \
   make && \
   make install
