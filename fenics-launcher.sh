#!/bin/bash
#OAR -l cpu=1/core=15/thread=1,walltime=5
#OAR --project project_rues
#OAR -t bigmem
#OAR -p for_rues='YES'
source $HOME/.profile
source $HOME/fenics-gaia-cluster/env-fenics.sh
time mpirun -x OPENBLAS_NUM_THREADS -x LD_LIBRARY_PATH -x PATH -x PYTHONPATH -hostfile ${OAR_NODEFILE} python poisson.py
