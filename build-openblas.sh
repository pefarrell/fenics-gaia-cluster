#!/bin/bash
source env-build-fenics.sh

OPENBLAS_VERSION="0.2.13"

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget -nc https://github.com/xianyi/OpenBLAS/archive/v${OPENBLAS_VERSION}.tar.gz -O openblas.tar.bz2 && \
   tar -xf openblas.tar.bz2 && \
   cd OpenBLAS-${OPENBLAS_VERSION} && \
   make -j ${BUILD_THREADS} PREFIX=${PREFIX} TARGET=SANDYBRIDGE USE_THREAD=0 && \
   make install PREFIX=${PREFIX}
