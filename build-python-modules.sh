#!/bin/bash
source env-build-fenics.sh

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

pip install petsc4py --install-option="--prefix=${PREFIX}"
pip install slepc4py --install-option="--prefix=${PREFIX}"
pip install ply --install-option="--prefix=${PREFIX}"
pip install six --install-option="--prefix=${PREFIX}"
pip install sympy --install-option="--prefix=${PREFIX}"
