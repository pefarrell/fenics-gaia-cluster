#!/bin/bash
source env-build-fenics.sh

EIGEN_VERSION="1.8.4"

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget -nc http://bitbucket.org/eigen/eigen/get/3.2.4.tar.bz2 -O eigen.tar.bz2 && \
   tar -xf eigen.tar.bz2 && \
   cd eigen-eigen-10219c95fe65 && \
   mkdir -p build && \
   cd build && \
   cmake ../ -DCMAKE_INSTALL_PREFIX=${PREFIX} && \
   make install
