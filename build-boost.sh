#!/bin/bash
source env-build-fenics.sh

mkdir -p ${BUILD_DIR}

cd ${BUILD_DIR} && \
   wget -nc http://sourceforge.net/projects/boost/files/boost/1.57.0/boost_1_57_0.tar.gz/download -O boost.tar.gz && \
   tar -xf boost.tar.gz && \
   cd boost_1_57_0 && \
   ./bootstrap.sh --prefix=$PREFIX --with-libraries=filesystem,program_options,system,thread,iostreams && \
   ./b2 -j ${BUILD_THREADS} install
