# fenics-gaia-cluster #
### Scripts to build FEniCS on the Gaia Cluster at the University of Luxembourg ###

These scripts will automatically build the latest stable version of [FEniCS](http://fenicsproject.org) with PETSc, SLEPc and HDF5 support on the gaia-80 from scratch. 
The only libraries used are the base Debian gcc toolchain.

You need to have an account on and be familiar with using the University of Luxembourg HPC first, see [HPC uni.lu](http://hpc.uni.lu)

### Compiling instructions ###

Conventions: `$` is a shell on the frontend, `$$` is a shell on a cluster machine (e.g. gaia-80).

gaia-80 has fast local HDD storage, so it is best to compile and install FEniCS there so as not to go out to the ${HOME} directory on NFS.
To do this you need to set the variable `${STORE}` in your `.profile` file:
```
#!shell
$ cd $HOME
$ echo "export STORE=/store/rues/${USER}" >> .bashrc
$ source .profile
```
Then clone this repository.
```
#!shell
$ cd $HOME
$ git clone git@bitbucket.org:unilucompmech/fenics-gaia-cluster.git
```
Run the following commands:
```
#!shell
$ cd $HOME
$ cd fenics-gaia-cluster
$ ./g80-small
$$ mkdir -p $STORE
$$ ./build-all.sh
```
Wait for the build to finish. This will take well over an hour.

When you want to run FEniCS you must reserve resources on the cluster and then setup your environment using:
```
#!shell
$$ source env-fenics.sh
```
before running any scripts.

### Advanced ###

You can adjust the build location and the installation location in the file `env-build-fenics.sh` 
Also you can adjust the location of your Instant JIT cache using the variables in the file `env-fenics.sh`.

### Running FEniCS MPI jobs on gaia-cluster ###

Included in this repository is a very simple example launcher script to submit jobs on the cluster. 
You might want to try the HPC team's launcher instead, it has many more useful features: 
[https://github.com/ULHPC/launcher-scripts/tree/devel/bash/MPI](https://github.com/ULHPC/launcher-scripts/tree/devel/bash/MPI).

```
#!shell
$ cd $HOME
$ cd fenics-gaia-cluster
$ oarsub -S fenics-launcher.sh
```
This will submit a job on 15 cores located on one CPU of gaia-80. You can see the output in the files `OAR-<JOBID>.stdout` and `OAR-<JOBID>.stderr`.

Note that the first time you run it you will need to wait for the JIT compiler to finish, so to get an accurate run-time you will need to run it twice. 
You can adjust the number of degrees of freedom by adjusting the variable `ndof` in `poisson.py`.
You can adjust the arguments used to submit the job by adjusting the #OAR directives in `fenics-launcher.sh`.
By default, each core will be assigned 1 million degrees of freedom and the walltime should be around 3 minutes.

### Known issues ###

- Stalling downloads on gaia-cluster: Wait for the timeout to occur and the download should resume.