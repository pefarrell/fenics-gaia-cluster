#!/bin/bash
source env-build-fenics.sh

VERSION="3.5.3"

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget -nc http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${VERSION}.tar.gz && \
   tar -xf petsc-${VERSION}.tar.gz && \
   cd petsc-${VERSION} && \
   ./configure --COPTFLAGS="-O2 -mtune=native -march=native" \
               --CXXOPTFLAGS="-O2 -mtune=native -march=native" \
	       --FOPTFLAGS="-O2 -mtune=native -march=native" \
               --with-blas-lib=${PREFIX}/lib/libopenblas.a --with-lapack-lib=${PREFIX}/lib/libopenblas.a \
	       --with-c-support \
	       --with-debugging=0 \
	       --with-shared-libraries \
	       --download-suitesparse \
	       --download-scalapack \
	       --download-metis \
	       --download-parmetis \
	       --download-ptscotch \
	       --download-hypre \
	       --download-mumps \
	       --download-blacs \
	       --prefix=${PREFIX} && \
    make && \
    make install
