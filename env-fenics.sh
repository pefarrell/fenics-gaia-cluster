#!/bin/bash
source env-build-fenics.sh

# Note: These must be unset initially, need to code this in somehow.
export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

# Turn off the MPI warning, apparently nothing to worry about
export OMPI_MCA_btl_openib_warn_default_gid_prefix=0
# Stops annoying error message from FEniCS instant compile
export OMPI_MCA_mpi_warn_on_fork=0
# Don't go out over the infiniband interconnect
export OMPI_MCA_btl=self,vader
# Try using cma copy mechanism
# Currently this doesn't work, see http://www.open-mpi.org/community/lists/users/2015/02/26356.php
#export OMPI_MCA_btl_vader_single_copy_mechanism=cma
# Make sure OpenBLAS only uses one thread
export OPENBLAS_NUM_THREADS=1

export INSTANT_CACHE_DIR=${PREFIX}/.instant
export INSTANT_ERROR_DIR=${PREFIX}/.instant
