#!/bin/bash
source env-build-fenics.sh

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   hg clone https://bitbucket.org/dolfin-adjoint/libadjoint libadjoint && \
   cd libadjoint && \
   mkdir build && \
   cd build && \
   cmake ../ -DCMAKE_INSTALL_PREFIX=${PREFIX} && \
   make && \
   make install
