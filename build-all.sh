#!/bin/bash
#OAR -l core=8/thread=1,walltime=5
#OAR --project project_rues
#OAR -t bigmem
#OAR -p for_rues='YES'
./build-openmpi.sh
./build-openblas.sh
./build-boost.sh
./build-hdf5.sh
./build-petsc.sh
./build-slepc.sh
./build-eigen.sh
./build-python-modules.sh
./build-fenics.sh
