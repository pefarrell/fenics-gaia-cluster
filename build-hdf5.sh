#!/bin/bash
source env-build-fenics.sh

HDF5_VERSION="1.8.14"

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget -nc http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-${HDF5_VERSION}.tar.bz2 -O hdf5.tar.bz2 && \
   tar -xf hdf5.tar.bz2 && \
   cd hdf5-${HDF5_VERSION} && \
   ./configure --enable-production --enable-parallel --enable-shared --prefix=${PREFIX} && \
   make -j ${BUILD_THREADS} && \
   make install
