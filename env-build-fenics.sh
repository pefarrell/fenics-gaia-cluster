#!/bin/bash
source ${HOME}/.profile

# Make sure you have created your /store directory at /store/rues/your-username on gaia-80
PREFIX=/store/rues/${USER}/fenics
BUILD_DIR=/store/rues/${USER}/build
BUILD_THREADS=8

export PATH=${PREFIX}/bin:${PATH}
export LD_LIBRARY_PATH=${PREFIX}/lib:${LD_LIBRARY_PATH}
export C_INCLUDE_PATH=${PREFIX}/include:${C_INCLUDE_PATH}
export CPLUS_INCLUDE_PATH=${PREFIX}/include:${CPLUS_INCLUDE_PATH}

export PYTHONPATH=${PREFIX}/lib/python2.7/site-packages:${PYTHONPATH}
