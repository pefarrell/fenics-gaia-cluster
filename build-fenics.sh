#!/bin/bash
source env-build-fenics.sh

FENICS_VERSION="1.5.0"

# The ProgramOptions did not link on the Easybuild Boost, need to build Boost as well.
# Follow directions on boost.org with --prefix=$HOME/stow/boost before running script.
mkdir -p $BUILD_DIR

cd $BUILD_DIR && \
    wget -nc https://bitbucket.org/fenics-project/ffc/downloads/ffc-${FENICS_VERSION}.tar.gz && \
    tar -xf ffc-${FENICS_VERSION}.tar.gz && \
    cd ffc-${FENICS_VERSION} && \
    python setup.py install --prefix=${PREFIX}

cd $BUILD_DIR && \
    wget -nc https://bitbucket.org/fenics-project/fiat/downloads/fiat-${FENICS_VERSION}.tar.gz && \
    tar -xf fiat-${FENICS_VERSION}.tar.gz && \
    cd fiat-${FENICS_VERSION} && \
    python setup.py install  --prefix=${PREFIX}

cd $BUILD_DIR && \
    wget -nc https://bitbucket.org/fenics-project/instant/downloads/instant-${FENICS_VERSION}.tar.gz && \
    tar -xf instant-${FENICS_VERSION}.tar.gz && \
    cd instant-${FENICS_VERSION} && \
    python setup.py install --prefix=${PREFIX}

cd $BUILD_DIR && \
    wget -nc https://bitbucket.org/fenics-project/ufl/downloads/ufl-${FENICS_VERSION}.tar.gz && \
    tar -xf ufl-${FENICS_VERSION}.tar.gz && \
    cd ufl-${FENICS_VERSION} && \
    python setup.py install --prefix=${PREFIX}

cd $BUILD_DIR && \
    wget -nc https://bitbucket.org/fenics-project/dolfin/downloads/dolfin-${FENICS_VERSION}.tar.gz && \
    tar -xf dolfin-${FENICS_VERSION}.tar.gz && \
    cd dolfin-${FENICS_VERSION} && \
    mkdir -p build && \
    cd build && \
    cmake ../ -DEIGEN3_INCLUDE_DIR=${PREFIX}/include/eigen3 -DDOLFIN_ENABLE_QT=False -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=${PREFIX} -DBOOST_ROOT=${PREFIX} -DBOOST_LIBRARYDIR=${PREFIX}/lib && \
    make -j ${BUILD_THREADS} && \
    make install
