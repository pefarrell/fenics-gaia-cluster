#!/bin/bash
source env-build-fenics.sh

OPENMPI_VERSION="1.8.4"

mkdir -p ${BUILD_DIR}

cd ${BUILD_DIR} && \
   wget -nc http://www.open-mpi.org/software/ompi/v1.8/downloads/openmpi-1.8.4.tar.gz -O openmpi.tar.gz && \
   tar -xf openmpi.tar.gz && \
   cd openmpi-${OPENMPI_VERSION} && \
   ./configure --prefix=${PREFIX} --with-cma && \
   make && \
   make -j ${BUILD_THREADS} && \
   make install
